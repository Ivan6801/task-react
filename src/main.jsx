import React from "react";
import ReactDOM from "react-dom/client";
import { AppUI } from "./App/AppUI";
import { TodoProvider } from "./TodoContext";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <TodoProvider>
      <AppUI />
    </TodoProvider>
  </React.StrictMode>
);

import "./TodoSearch.css";
import { useContext } from "react";
import { TodoContext } from "../TodoContext";

export function TodoSearch() {
  const { searchValue, setSearchValue } = useContext(TodoContext);
  return (
    <>
      <input
        placeholder="Cortar cebolla"
        className="TodoSearch"
        value={searchValue}
        onChange={(event) => {
          setSearchValue(event.target.value);
        }}
      />
    </>
  );
}
